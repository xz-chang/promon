## How to decompile
I've used JD-GUI to decompile the code. Obviously the .class file was not obfuscated at all. So it's not diffult to figure out the key encryption key is as following.

```%Sup3rSecretKey!```

## How to fix this security loophole
 - The safest solution is always store them in the server side.
 - Obfuscation could be helpful to map the human-friendly symbols into just less meaningful names, but still it's not enough.
 - Put the secret in *C* code. Handling *assembly code* is more demanding
 
## Android specific - Android keystore system - they are designed for this purpose. The whole process would be
 - Client pass a specific authentication process
 - Save the key into the keystore
 - Fetch it whenerver it is needed.

## Benefit of using keystore
  - Key material never enters the application process
  - Key material might be bound to the secure hardware, such as sim card and embedded Secure Element
 
## References
  - https://developer.android.com/training/articles/keystore
  - https://github.com/codepath/android_guides/wiki/Storing-Secret-Keys-in-Android#secrets-in-resource-files
