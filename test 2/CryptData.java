import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class CryptData
{
  public byte[] encrypt(byte[] paramArrayOfByte)
    throws NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException
  {
    SecureRandom localSecureRandom = new SecureRandom();
    SecretKeySpec localSecretKeySpec = new SecretKeySpec("%Sup3rSecretKey!".getBytes(), "AES");
    byte[] arrayOfByte1 = new byte[16];
    localSecureRandom.nextBytes(arrayOfByte1);
    
    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    localCipher.init(1, localSecretKeySpec, new IvParameterSpec(arrayOfByte1), localSecureRandom);
    byte[] arrayOfByte2 = localCipher.doFinal(paramArrayOfByte);
    
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(arrayOfByte1.length + arrayOfByte2.length);Object localObject1 = null;
    try
    {
      localByteArrayOutputStream.write(arrayOfByte1);
      localByteArrayOutputStream.write(arrayOfByte2);
      return localByteArrayOutputStream.toByteArray();
    }
    catch (Throwable localThrowable1)
    {
      localObject1 = localThrowable1;throw localThrowable1;
    }
    finally
    {
      if (localByteArrayOutputStream != null) {
        if (localObject1 != null) {
          try
          {
            localByteArrayOutputStream.close();
          }
          catch (Throwable localThrowable3)
          {
            ((Throwable)localObject1).addSuppressed(localThrowable3);
          }
        } else {
          localByteArrayOutputStream.close();
        }
      }
    }
  }
}

