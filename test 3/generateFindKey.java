import java.util.*;

public class generateFindKey
{

	static String lowerKeys[];
	static {
		lowerKeys = new String[Keys.keys.length];
		for ( int i=0 ; i < Keys.keys.length ; i++ )
			lowerKeys[i] = Keys.keys[i].toLowerCase();
	}

	static public void main(String[] args) {
		// Partition the keys into buckets where each string has the same length
		HashMap<Integer,ArrayList<String>> buckets = new HashMap<Integer,ArrayList<String>>();
		for ( String key : lowerKeys ) {
			int l = key.length();
			if (!buckets.containsKey(l))
				buckets.put(l, new ArrayList<String>());
			buckets.get(l).add(key);
		}

		P("/* BEGIN GENERATED CODE */");
		P("/* Do not edit this code. */");
		P("static KeyValue findKey(const char* k, int keylen)");
		P("{");
		I();
		P("int checksum=0x47275664, i;");
		P("for ( i=0 ; i < keylen ; i++ )");
		I();
		P("checksum = (checksum << 1) + (TOLOWER(k[i])-32);");
		U();
		P("checksum = checksum & 0x7FFFFFFF;");
		// The outermost dispatch is on the key length.
		P("switch (keylen) {");
		for ( Map.Entry<Integer,ArrayList<String>> entry : buckets.entrySet() ) {
			P("case " + entry.getKey() + ":");
			I();
			discriminate(entry.getValue());
			U();
		}
		P("default:");
		I();
		P("break;");
		U();
		P("}");
		P("return K_NOTFOUND;");
		U();
		P("}");
		P("/* END GENERATED CODE */");
	}

	// The keys all have the same length, so we need to discriminate
	// on their names.
	//
	// If there's only one key then it's the key we want, and we're done.
	//
	// Otherwise, find the character that partitions the strings into the most 
	// buckets, and switch on that first.  Then repeat.  Unless the set is very
	// dense this reveals little about the key names.

	static void discriminate(ArrayList<String> keys) {
		String k0 = keys.get(0);
		int len = k0.length();
		if (keys.size() == 1) {
			P("if (checksum == " + checkSum(k0) + ") return K_" + originalName(k0) + ";");
			P("break;");
			return;
		}
		
		int bestIndex = -1;
		int bestCardinality = -1;
		for ( int i=0 ; i < len ; i++ ) {
			HashSet<Character> chars = new HashSet<Character>();
			for ( String key : keys ) 
				chars.add(key.charAt(i));
			if (chars.size() > bestCardinality) {
				bestCardinality = chars.size();
				bestIndex = i;
			}
		}

		HashMap<Character,ArrayList<String>> buckets = new HashMap<Character,ArrayList<String>>();
		for ( String key : keys ) {
			char c = key.charAt(bestIndex);
			if (!buckets.containsKey(c))
				buckets.put(c, new ArrayList<String>());
			buckets.get(c).add(key);
		}

		P("switch (TOLOWER(k[" + bestIndex + "])) {");
		for ( Map.Entry<Character,ArrayList<String>> entry : buckets.entrySet() ) {
			P("case '" + entry.getKey() + "':");
			I();
			discriminate(entry.getValue());
			U();
		}
		P("default:");
		I();
		P("break;");
		U();
		P("}");
		P("break;");
	}

	static int checkSum(String key) {
		int checksum=0x47275664;
		for ( int i=0 ; i < key.length() ; i++ )
			checksum = (checksum << 1) + (key.charAt(i)-32);
		return checksum & 0x7FFFFFFF;
	}

	static String originalName(String lowerKey) {
		for ( int i=0 ; i < lowerKeys.length ; i++ )
			if (lowerKeys[i].equals(lowerKey))
				return Keys.keys[i];
		throw new Error("Key not found: " + lowerKey);
	}

	static String indent = "";

	static void I() {
		indent += "    ";
	}

	static void U() {
		indent = indent.substring(4);
	}

	static void P(String s) {
		System.out.println(indent + s);
	}
}
