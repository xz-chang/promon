package no.promon.test.jarminimizer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import javassist.NotFoundException;

public class Main {

	public static void main(String[] args) {
		Options options = new Options();
		
        Option input = new Option("i", "input", true, "input file path");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("o", "output", true, "output file");
        output.setRequired(true);
        options.addOption(output);
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;
        
        try {
			cmd = parser.parse(options,  args);
		} catch (ParseException e1) {
			System.out.println(e1.getMessage());
            formatter.printHelp("JarRewriter", options);
			System.exit(1);
		}
        
        String inputFilePath = cmd.getOptionValue("input");
        String outputFilePath = cmd.getOptionValue("output");
       
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        System.out.println("inputFilePath = " + inputFilePath);
        System.out.println("outputFilePath = " + outputFilePath);
        
        try {
			rewrite(inputFilePath, outputFilePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void rewrite(String inputFilePath, String outputFilePath) throws FileNotFoundException, IOException, NotFoundException {
		JarRewriter rewriter = new JarRewriterImpl();
		File f = new File(inputFilePath);
		System.out.println("File dir = " + f.getAbsolutePath());
		if (f.exists()) {
			System.out.println("File exist");
		} else {
			System.out.println("File not found!");
		}
		
		JarInputStream jis = new JarInputStream(new BufferedInputStream(new FileInputStream(inputFilePath)));
		JarOutputStream jos = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(outputFilePath)), jis.getManifest());
		
		try {
			rewriter.rewrite(jis, jos);
		} finally {
			jis.close();
			jos.close();
			
		}
		
	}

}
