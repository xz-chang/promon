package no.promon.test.jarminimizer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.zip.Deflater;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

public class JarRewriterImpl implements JarRewriter {

	
	@Override
	public void rewrite(JarInputStream jarInput, JarOutputStream jarOutput) throws IOException {
		try {
			_rewrite(jarInput, jarOutput);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotCompileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void _rewrite(JarInputStream jarInput, JarOutputStream jarOutput) throws IOException, NotFoundException, CannotCompileException {
		ClassPool cp0 = ClassPool.getDefault();
		
		// Should have use BufferedInputStream for performance concerns!
		jarOutput.setLevel(Deflater.BEST_COMPRESSION);
		byte[] bytes = new byte[4096];
		JarEntry entryTemp = null;
		while((entryTemp = jarInput.getNextJarEntry()) != null) {
			// jarOutput.putNextEntry(entryTemp); Wrong! it will also copy the compressed size
			// refer to https://stackoverflow.com/questions/13435683/invalid-entry-compressed-size
			final String entryName = entryTemp.getName();
			if (!entryName.endsWith(".class") || !entryName.contains("promon")) {
//				System.out.println("Not a valid class + " + entryName + "\n");
				continue;
			}
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int realRead = jarInput.read(bytes, 0, bytes.length);
            while(realRead != -1) {
            	baos.write(bytes, 0, realRead);
                realRead = jarInput.read(bytes, 0, bytes.length);
            }
            
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            
			CtClass cc = cp0.makeClass(bais);
			System.out.println("CtClass: + " + cc.getName() + "\n");
			for (CtMethod method: cc.getDeclaredMethods()) {
				System.out.println("\tCtMethod: + " + method.getName() + "\n");
				method.setBody(null); // setBody(null) will make sure it the body is empty
			}
			
			byte[] newClassByteCode = cc.toBytecode();
			
			jarOutput.putNextEntry(new JarEntry(entryName));
			jarOutput.write(newClassByteCode);			
			jarOutput.closeEntry();
		}
		
		jarOutput.flush();
		
		jarOutput.finish();
		
		System.out.println("rewrite completed");
	}

}
