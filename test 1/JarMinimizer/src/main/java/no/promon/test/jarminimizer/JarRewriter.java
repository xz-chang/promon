package no.promon.test.jarminimizer;

import java.io.IOException;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

/**
 *
 * @author janvidar
 */
public interface JarRewriter
{
    /**
     * Parse the Jar-file from the jarInput and re-create a similar jar file
     * in the specified jarOutput.
     * 
     * @param jarInput the original Jar-file input stream
     * @param jarOutput the new Jar-file output stream.
     * @throws java.io.IOException if reading from jarInput or writing to jarOutput fails.
     */
    void rewrite(JarInputStream jarInput, JarOutputStream jarOutput) throws IOException;
}
