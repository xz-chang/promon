## How to run
 - Go to the *root* of the project
 - Modify the input output parameter in pom.xml
 - Execute ``mvn clean compile exec:java``

## Explanation for Question 1 task 1
 - Have to explicitly copy the Manifest, otherwise it's not copied.
 - I was using beyond compare to compare the `intput.jar` and `output.jar`
 - A tag ``question1-task2-completed`` was added

## Explanation for Question 1 task 2
 - My initial thought for this task was to use ClassLoader and/or parse the .class file. But this is too much work
 - So I've decided to use javassist to modify the java code.
 - I've jused ``javap -c Main.class`` to verify that the functions only returns null/0
 
```
C:\Users\Chang\git-root\promon\test 1\JarMinimizer\target\output\no\promon\test\jarminimizer>javap -c Main.class
Compiled from "Main.java"
public class no.promon.test.jarminimizer.Main {
  public no.promon.test.jarminimizer.Main();
    Code:
       0: aload_0
       1: invokespecial #14                 // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: return
}```
